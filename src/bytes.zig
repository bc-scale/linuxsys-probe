const ascii = std.ascii;
const c = @import("c.zig");
const fmt = std.fmt.allocPrint;
const mem = std.mem;
const std = @import("std");
const testing = std.testing;

const Allocator = mem.Allocator;
const Str = []const u8;

const prefixes = [_]Str{
    "B",
    "KiB",
    "MiB",
    "GiB",
    "TiB",
    "PiB",
    "EiB",
    "ZiB",
    "YiB",
};

pub fn toString(alloc: *Allocator, bytes: u64) !Str {
    return fmt(alloc, "{:.2}", .{std.fmt.fmtIntSizeBin(bytes)});
}

pub fn fromString(text: Str) u64 {
    var value: f32 = 1;
    var i: usize = 1;
    while (i < text.len) : (i += 1) {
        const opt_float = std.fmt.parseFloat(f32, text[0..i]);
        if (opt_float) |f| {
            value = f;
        } else |_| {
            var multiplier: f32 = 1;
            for (prefixes) |prefix| {
                if (mem.eql(u8, prefix, text[i - 1 ..])) {
                    value *= multiplier;
                    break;
                }
                multiplier *= 1024;
            }
            break;
        }
    }
    return @floatToInt(u64, value);
}

test "toString" {
    const alloc = testing.allocator;

    const str = try toString(alloc, 1024);
    defer alloc.free(str);
    try testing.expectEqualStrings("1.00KiB", str);
}

test "fromString" {
    try testing.expectEqual(fromString("1KiB"), 1024);
    try testing.expectEqual(fromString("10B"), 10);
}
