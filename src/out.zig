const Allocator = std.mem.Allocator;
const ArenaAllocator = std.heap.ArenaAllocator;
const bytes = @import("bytes.zig");
const debug = std.debug;
const fmt = std.fmt.allocPrint;
const std = @import("std");
const testing = std.testing;
const time = std.time;

const Options = @import("options.zig").Options;
const Str = []const u8;

pub const Unit = enum {
    none,
    byte,
    second,
};

fn fmtValue(alloc: *Allocator, value: Str, unit: Unit) !Str {
    return switch (unit) {
        .none => value,
        .byte => byte: {
            const val = try std.fmt.parseInt(u64, value, 10);
            break :byte bytes.toString(alloc, val);
        },
        .second => second: {
            const val_ns = try std.fmt.parseInt(u64, value, 10);
            const val = val_ns * time.ns_per_s;
            break :second try fmt(alloc, "{}", .{std.fmt.fmtDuration(val)});
        },
    };
}

fn fmtLine(
    alloc: *Allocator,
    key: Str,
    opt_key_note: ?Str,
    value: Str,
    value_unit: Unit,
    opt_value_note: ?Str,
    options: Options,
) !Str {
    var key_note: Str = "";
    if (options.verbose) {
        if (opt_key_note) |note| {
            key_note = try fmt(alloc, " ({s})", .{note});
        }
    }

    var value_note: Str = "";
    if (options.notes) {
        if (opt_value_note) |note| {
            value_note = try fmt(alloc, " ({s})", .{note});
        }
    }

    if (options.no_humanize) {
        return try fmt(alloc, "{s} {s}\n", .{ key, value });
    }

    const formatted = fmtValue(alloc, value, value_unit) catch value;
    return try fmt(
        alloc,
        "{s}{s} = {s}{s}\n",
        .{ key, key_note, formatted, value_note },
    );
}

pub fn println(
    allocator: *Allocator,
    key: Str,
    opt_key_note: ?Str,
    value: Str,
    value_unit: Unit,
    opt_value_note: ?Str,
    options: Options,
) !void {
    var arena = ArenaAllocator.init(allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    const line = try fmtLine(
        alloc,
        key,
        opt_key_note,
        value,
        value_unit,
        opt_value_note,
        options,
    );
    debug.print("{s}", .{line});
}

test "fmtLine" {
    const allocator = testing.allocator;

    var arena = ArenaAllocator.init(allocator);
    defer arena.deinit();
    const alloc = &arena.allocator;

    var arg = [_][*:0]const u8{ "prog", "-v", "-n" };
    const options = try Options.argInit(
        alloc,
        arg.len,
        @ptrCast([*][*:0]u8, &arg),
    );
    const line = try fmtLine(alloc, "k", "kn", "v", Unit.none, "vn", options);
    try testing.expectEqualStrings("k (kn) = v (vn)\n", line);

    const byte_line = try fmtLine(
        alloc,
        "k",
        "kn",
        "1024",
        Unit.byte,
        "vn",
        options,
    );
    try testing.expectEqualStrings("k (kn) = 1.00KiB (vn)\n", byte_line);

    const secs_line = try fmtLine(
        alloc,
        "k",
        "kn",
        "60",
        Unit.second,
        "vn",
        options,
    );
    try testing.expectEqualStrings("k (kn) = 1m (vn)\n", secs_line);
}
