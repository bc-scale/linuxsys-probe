// NOTE: Zig 0.8.1 doesn't allow to use struct_sysinfo,
// if use `@cInclude("sys/sysinfo.h");`.
// The nightly version has some issues too (could be related to `__reserved`
// having size 255 instead of the proper padding), therefore
// manually declaring the C definitions
const use_sysinfo_cimport = false;

pub usingnamespace @cImport({
    @cDefine("_GNU_SOURCE", {});
    @cInclude("getopt.h");
    @cInclude("sched.h");
    @cInclude("stdio.h");
    @cInclude("stdlib.h");
    @cInclude("unistd.h");
    @cInclude("sys/wait.h");
    if (use_sysinfo_cimport) {
        @cInclude("sys/sysinfo.h");
    }
});

pub usingnamespace switch (use_sysinfo_cimport) {
    true => struct {},
    false => struct {
        const padding = 20 - 2 * @sizeOf(c_long) - @sizeOf(c_int);
        pub const struct_sysinfo = extern struct {
            uptime: c_ulong,
            loads: [3]c_ulong,
            totalram: c_ulong,
            freeram: c_ulong,
            sharedram: c_ulong,
            bufferram: c_ulong,
            totalswap: c_ulong,
            freeswap: c_ulong,
            procs: c_ushort,
            pad: c_ushort,
            totalhigh: c_ulong,
            freehigh: c_ulong,
            mem_unit: c_uint,
            __reserved: [padding]u8,
        };
        pub extern fn sysinfo([*c]struct_sysinfo) c_int;
        pub const SI_LOAD_SHIFT = @as(c_int, 16);
    },
};
